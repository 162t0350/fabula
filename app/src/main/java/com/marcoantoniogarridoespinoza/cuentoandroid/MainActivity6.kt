package com.marcoantoniogarridoespinoza.cuentoandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button

class MainActivity6 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main6)

        val boton6=findViewById<Button>(R.id.boton6)
        boton6.setOnClickListener {
            val intento6 = Intent(this, MainActivity7::class.java)
            startActivity(intento6)
        }
    }
}