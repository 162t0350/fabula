package com.marcoantoniogarridoespinoza.cuentoandroid

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity10 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main10)

        val boton10 =findViewById<Button>(R.id.boton10)
        boton10.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
//Open next activity
//Open next activity
            startActivity(intent)

        }

    }
}