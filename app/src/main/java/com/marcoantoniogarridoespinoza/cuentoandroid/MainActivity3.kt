package com.marcoantoniogarridoespinoza.cuentoandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button

class MainActivity3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        val boton3=findViewById<Button>(R.id.boton3)
        boton3.setOnClickListener {
            val intento3 = Intent(this, MainActivity4::class.java)
            startActivity(intento3)
        }
    }
}