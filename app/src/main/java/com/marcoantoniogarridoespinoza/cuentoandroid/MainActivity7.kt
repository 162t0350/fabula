package com.marcoantoniogarridoespinoza.cuentoandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button

class MainActivity7 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main7)

        val boton7=findViewById<Button>(R.id.boton7)
        boton7.setOnClickListener {
            val intento7 = Intent(this, MainActivity8::class.java)
            startActivity(intento7)
        }
    }
}