package com.marcoantoniogarridoespinoza.cuentoandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button

class MainActivity9 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main9)

        val boton9=findViewById<Button>(R.id.boton9)
        boton9.setOnClickListener {
            val intento9 = Intent(this, MainActivity10::class.java)
            startActivity(intento9)
        }
    }
}