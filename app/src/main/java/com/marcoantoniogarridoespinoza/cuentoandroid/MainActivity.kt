package com.marcoantoniogarridoespinoza.cuentoandroid

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

const val EXTRA_MESSAGE = "com.marcoantoniogarridoespinoza.cuentoandroid"
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fun sendMessage(view: View) {
            val editText = findViewById<EditText>(R.id.editTextTextPersonName)
            val message = editText.text.toString()
            val intent = Intent(this, MainActivity2::class.java).apply {
                putExtra(EXTRA_MESSAGE, message)
            }
            startActivity(intent)
        }

        val boton1=findViewById<Button>(R.id.boton1)
        boton1.setOnClickListener {

            val intento1 = Intent(this, MainActivity2::class.java)
            startActivity(intento1)
        }
        val salir=findViewById<Button>(R.id.salir)
        salir.setOnClickListener {
            finishAffinity()
        }
    }
}