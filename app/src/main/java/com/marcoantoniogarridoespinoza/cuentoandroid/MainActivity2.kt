package com.marcoantoniogarridoespinoza.cuentoandroid

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

// Get the Intent that started this activity and extract the string
        val message = intent.getStringExtra(EXTRA_MESSAGE)

        // Capture the layout's TextView and set the string as its text
        val textView = findViewById<TextView>(R.id.textView4).apply {
            text = message
        }

        val boton2=findViewById<Button>(R.id.boton2)
        boton2.setOnClickListener {
            val intento2 = Intent(this, MainActivity3::class.java)
            startActivity(intento2)
        }

    }
}