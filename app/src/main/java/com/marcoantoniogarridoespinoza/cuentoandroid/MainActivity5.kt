package com.marcoantoniogarridoespinoza.cuentoandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button
class MainActivity5 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main5)

        val boton5=findViewById<Button>(R.id.boton5)
        boton5.setOnClickListener {
            val intento5 = Intent(this, MainActivity6::class.java)
            startActivity(intento5)
        }
    }
}