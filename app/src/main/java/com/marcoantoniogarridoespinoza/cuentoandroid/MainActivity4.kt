package com.marcoantoniogarridoespinoza.cuentoandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button

class MainActivity4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main4)

        val boton4=findViewById<Button>(R.id.boton4)
        boton4.setOnClickListener {
            val intento4 = Intent(this, MainActivity5::class.java)
            startActivity(intento4)
        }
    }
}