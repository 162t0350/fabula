package com.marcoantoniogarridoespinoza.cuentoandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button

class MainActivity8 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main8)

        val boton8=findViewById<Button>(R.id.boton8)
        boton8.setOnClickListener {
            val intento8 = Intent(this, MainActivity9::class.java)
            startActivity(intento8)
        }
    }
}